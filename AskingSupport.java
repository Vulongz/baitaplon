import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class AskingSupport extends JFrame implements ActionListener {
    private JPanel AskingSupport;
    private JProgressBar progressBar1;
    private JProgressBar progressBar2;
    private JProgressBar progressBar3;
    private JProgressBar progressBar4;
    private JLabel pa;
    private JLabel pb;
    private JLabel pc;
    private JLabel pd;
    private JButton OKButton;

    AskingSupport(Question q) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(220, 150);
        setResizable(false);
        setContentPane(AskingSupport);
        OKButton.addActionListener(this);

        Random r = new Random();
        int x = r.nextInt(10);

        if (x > 4) x = 20 + r.nextInt(81);
        else  x = r.nextInt(100);

        for (int i = 0; i < 4; i++) {
            if (q.getAns()[i].equals(q.getCorrectAnswer())) {
                if (i == 0) {
                    progressBar1.setValue(x);
                    pa.setText(x + "%");
                } else if (i == 1) {
                    progressBar2.setValue(x);
                    pb.setText(x + "%");
                } else if (i == 2) {
                    progressBar3.setValue(x);
                    pc.setText(x + "%");
                } else {
                    progressBar4.setValue(x);
                    pd.setText(x + "%");
                }
            }
        }

        for(int i = 0; i < 4; i++) {
            int temp;
            if(!q.getAns()[i].equals(q.getCorrectAnswer())) {
                temp = r.nextInt(100-x);
                x += temp;
                if (i == 0) {
                    progressBar1.setValue(temp);
                    pa.setText(temp + "%");
                } else if (i == 1) {
                    progressBar2.setValue(temp);
                    pb.setText(temp + "%");
                } else if (i == 2) {
                    progressBar3.setValue(temp);
                    pc.setText(temp + "%");
                } else {
                    progressBar4.setValue(temp);
                    pd.setText(temp + "%");
                }
            }
        }

        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == OKButton) this.dispose();
    }
}
