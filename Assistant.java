import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Assistant {

    private ArrayList<Question> questions = new ArrayList<Question>();
    private Question currentQuestion;
    private int score = 0;
    private int answered = 0;

    void scoreUp() {
        this.score++;
    }

    int getScore() {
        return this.score;
    }

    void getAllQuestions() throws IOException {
        String[] line = new String[5];
        int i = 0;
        BufferedReader br = null;
        br = new BufferedReader(new FileReader("data.txt"));
        while ((line[i] = br.readLine()) != null) {
            i++;
            if (i == 5) {
                i = 0;
                questions.add(new Question(line[0], line[1], line[2], line[3], line[4]));
            }
        }
    }

    int getHighScore() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("highscore.txt"));
        int highscore = 0;
        while(sc.hasNextInt()) {
            highscore = sc.nextInt();
        }
        return highscore;
    }

    void saveHighScore(int score) throws FileNotFoundException {
        BufferedWriter br = null;
        try {
            File file = new File("highscore.txt");
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            br = new BufferedWriter(fw);
            br.newLine();
            br.write(String.valueOf(score));
            br.close();
        } catch (java.io.IOException e1) {
            e1.printStackTrace();
        }
    }

    void shuffleQuestions() {
        Collections.shuffle(questions);
    }

    Question getTheCurrentQuestion() {
        currentQuestion = questions.get(answered);
        currentQuestion.shuffleAnswers();
        answered++;
        return currentQuestion;
    }

    int getNumberQuestions() {
        return this.questions.size();
    }

    boolean check(String answer) {
        return answer.equals(currentQuestion.getCorrectAnswer());
    }

}
