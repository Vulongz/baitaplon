import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends JFrame implements ActionListener {
    private JButton exitButton;
    private JPanel Main;
    private JButton adminButton;
    private JButton playButton;

    public Main() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Main menu");
        setResizable(false);
        setSize(300, 300);
        setContentPane(Main);
        playButton.addActionListener(this);
        adminButton.addActionListener(this);
        exitButton.addActionListener(this);
        playButton.setPreferredSize(new Dimension(200, 40));
        adminButton.setPreferredSize(new Dimension(200, 40));
        exitButton.setPreferredSize(new Dimension(200, 40));
        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == playButton) {
            this.dispose();
            new Start();
        }
        if (e.getSource() == adminButton) {
            this.dispose();
            new Login();
        }
        if (e.getSource() == exitButton) {
            this.dispose();
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception ignored) {
        }
        new Main();
    }
}
