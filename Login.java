import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lumosnysm on 23/07/2017.
 */
public class Login extends JFrame implements ActionListener {
    private JPanel Login;
    private JPasswordField passwordField1;
    private JButton loginButton;

    private String password = "admin";

    Login() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Login");
        setSize(200, 100);
        setContentPane(Login);
        loginButton.addActionListener(this);
        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String input = new String(passwordField1.getPassword());
            if (input.equals(password)) {
                this.dispose();
                new Admin();
            } else {
                JOptionPane.showMessageDialog(this, "Wrong password");
                this.dispose();
                new Login();
            }
        }
    }
}
