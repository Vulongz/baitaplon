import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lumosnysm on 23/07/2017.
 */
public class End extends JFrame implements ActionListener {
    private JPanel End;
    private JTextField textField1;
    private JButton playAgainButton;
    private JButton exitButton;
    private JLabel label;

    private int score;

    End(int score, boolean win) {
        this.score = score;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(300, 300);
        setContentPane(End);
        playAgainButton.addActionListener(this);
        exitButton.addActionListener(this);
        if (win) label.setText("You Win");
        else label.setText("You Lose");
        textField1.setText("Your score: " + score);
        playAgainButton.setPreferredSize(new Dimension(200, 40));
        exitButton.setPreferredSize(new Dimension(200, 40));
        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == exitButton) this.dispose();
        if (e.getSource() == playAgainButton) {
            this.dispose();
            new Start();
        }
    }
}
